import 'package:flutter/material.dart';

class FocusPainter extends CustomPainter {
  final Offset point;

  FocusPainter({required this.point});

  @override
  void paint(Canvas canvas, Size size) {
    const squareSize = 60.0;
    final squareOffset = Offset(point.dx - squareSize / 2, point.dy - squareSize / 2);
    final squareRect = Rect.fromLTWH(squareOffset.dx, squareOffset.dy, squareSize, squareSize);

    final paint = Paint()
      ..color = Colors.amberAccent
      ..style = PaintingStyle.stroke
      ..strokeWidth = 2.0;

    canvas.drawRect(squareRect, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }
}