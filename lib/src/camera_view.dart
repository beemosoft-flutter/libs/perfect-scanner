import 'dart:async';
import 'dart:io';

import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_mlkit_barcode_scanning/google_mlkit_barcode_scanning.dart';
import 'package:perfect_scanner/src/focus_painter.dart';
import 'package:perfect_scanner/src/image_handler.dart';
import 'package:perfect_scanner/src/scanner_controller.dart';
import 'overlay.dart';

/// Widget for scanner.
///
/// It uses `camera` package to capture the qr and google ml kit for process the image.
class ScannerView extends StatefulWidget {
  const ScannerView({
    Key? key,
    required this.onScan,
    this.qrOverlay,
    this.hasFocus,
  }) : super(key: key);

  /// Overlay for qr widget
  final QrOverlay? qrOverlay;

  /// This function will return a string from the captured qr code from camera.
  final Function(String inputImage) onScan;

  final bool? hasFocus;

  @override
  ScannerViewState createState() => ScannerViewState();
}

class ScannerViewState extends State<ScannerView> with WidgetsBindingObserver {
  List<CameraDescription> cameras = [];
  int _cameraIndex = 0;
  double zoomLevel = 0.0, minZoomLevel = 0.0, maxZoomLevel = 0.0;

  CameraLensDirection cameraLensDirection = CameraLensDirection.back;

  bool _canProcess = true;
  bool _isBusy = false;
  String? _text;
  Offset? _tapPosition;
  bool _showFocusOverlay = false;
  // BarcodeScanner barcodeScanner = BarcodeScanner(
  //   formats: [BarcodeFormat.qrCode],
  // );

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getCamera().then((value) {
      if (cameras.any(
        (element) => element.sensorOrientation == 90,
      )) {
        _cameraIndex = cameras.indexOf(
          cameras.firstWhere((element) => element.sensorOrientation == 90),
        );
      }
      startLiveFeed();
    });
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.resumed:
        print('didChangeAppLifecycleState resumed');
        startLiveFeed();
        break;
      case AppLifecycleState.inactive:
        print('didChangeAppLifecycleState inactive');
        break;
      case AppLifecycleState.paused:
        print('didChangeAppLifecycleState paused');
        disposeControllers();
        // stopPingWebsocket();
        break;
      case AppLifecycleState.detached:
        print('didChangeAppLifecycleState detached');
        break;
    }
    super.didChangeAppLifecycleState(state);
  }

  @override
  void dispose() async {
    WidgetsBinding.instance.removeObserver(this);
    _canProcess = false;
    disposeControllers();
    super.dispose();
  }

  /// Disposing the camera controllers.
  void disposeControllers() async {
    try {
      await ScannerController().dispose();
      ImageHandler().dispose();
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  /// To get the available cameras from camera library
  Future<void> getCamera() async {
    cameras = await availableCameras();
  }

  @override
  Widget build(BuildContext context) {
    if (ScannerController().controller == null || !ScannerController().controller!.value.isInitialized) {
      return Container(
        color: Colors.black,
      );
    }

    var camera = ScannerController().controller!.value;
    // fetch screen size
    final size = MediaQuery.of(context).size;

    // calculate scale depending on screen and camera ratios
    // this is actually size.aspectRatio / (1 / camera.aspectRatio)
    // because camera preview size is received as landscape
    // but we're calculating for portrait orientation
    var scale = size.aspectRatio * camera.aspectRatio;

    // to prevent scaling down, invert the value
    if (scale < 1) scale = 1 / scale;
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Transform.scale(
            scale: scale,
            child: Center(
              child: CameraPreview(ScannerController().controller!),
            ),
          ),
          if (widget.qrOverlay != null)
            Center(
              child: GestureDetector(
                onTapDown: (details) {
                  ScannerController().takeFocus(details, context);
                  if (widget.hasFocus == true) {
                    setState(() {
                      _tapPosition = details.globalPosition;
                    });
                    _showFocusOverlay = true;
                    Timer(const Duration(seconds: 1), () {
                      setState(() {
                        _showFocusOverlay = false;
                      });
                    });
                  }
                },
                child: Stack(
                  children: [
                    Container(
                      decoration: ShapeDecoration(
                        shape: widget.qrOverlay!,
                      ),
                    ),
                    if (_tapPosition != null && _showFocusOverlay)
                      CustomPaint(
                        painter: FocusPainter(
                          point: _tapPosition!,
                        ),
                      ),
                  ],
                ),
              ),
            ),
        ],
      ),
    );
  }

  /// Starting the image process as soon as camera captured any barcode
  Future startLiveFeed() async {
    CameraDescription camera = cameras[_cameraIndex];
    CameraController controller = CameraController(
      camera,
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );
    ScannerController().setCameraController(controller);
    ScannerController().controller?.initialize().then((_) {
      if (!mounted) {
        return;
      }
      ScannerController().controller?.getMinZoomLevel().then((value) {
        zoomLevel = value;
        minZoomLevel = value;
      });
      ScannerController().controller?.getMaxZoomLevel().then((value) {
        maxZoomLevel = value;
      });
      ScannerController().controller?.startImageStream((value) {
        _processCameraImage(value);
      });

      setState(() {});
    });
  }

  Future<void> _processImage(InputImage inputImage) async {
    if (!_canProcess) return;
    if (_isBusy) return;
    _isBusy = true;
    setState(() {
      _text = '';
    });
    String text = await ImageHandler().processImage(inputImage);
    // String text = '';
    // for (final barcode in barcodes) {
    //   text += barcode.rawValue ?? '';
    // }
    _text = text;
    _isBusy = false;
    if (_text != null && _text!.isNotEmpty) {
      widget.onScan(_text!);
    }
    if (mounted) {
      setState(() {});
    }
  }

  /// Process the image from the camera and get the qr from it, and it will attach the detected qr to `onScan` function.
  Future<void> _processCameraImage(CameraImage image) async {
    final inputImage = _inputImageFromCameraImage(image);
    if (inputImage == null) return;
    await _processImage(inputImage);
  }

  final _orientations = {
    DeviceOrientation.portraitUp: 0,
    DeviceOrientation.landscapeLeft: 90,
    DeviceOrientation.portraitDown: 180,
    DeviceOrientation.landscapeRight: 270,
  };

  InputImage? _inputImageFromCameraImage(CameraImage image) {

    // get image rotation
    // it is used in android to convert the InputImage from Dart to Java: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/android/src/main/java/com/google_mlkit_commons/InputImageConverter.java
    // `rotation` is not used in iOS to convert the InputImage from Dart to Obj-C: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/google_mlkit_commons/ios/Classes/MLKVisionImage%2BFlutterPlugin.m
    // in both platforms `rotation` and `camera.lensDirection` can be used to compensate `x` and `y` coordinates on a canvas: https://github.com/flutter-ml/google_ml_kit_flutter/blob/master/packages/example/lib/vision_detector_views/painters/coordinates_translator.dart
    final camera = cameras[_cameraIndex];
    final sensorOrientation = camera.sensorOrientation;
    // print(
    //     'lensDirection: ${camera.lensDirection}, sensorOrientation: $sensorOrientation, ${_controller?.value.deviceOrientation} ${_controller?.value.lockedCaptureOrientation} ${_controller?.value.isCaptureOrientationLocked}');
    InputImageRotation? rotation;
    if (Platform.isIOS) {
      rotation = InputImageRotationValue.fromRawValue(sensorOrientation);
    } else if (Platform.isAndroid) {
      var rotationCompensation =
      _orientations[ScannerController().controller!.value.deviceOrientation];
      if (rotationCompensation == null) return null;
      if (camera.lensDirection == CameraLensDirection.front) {
        // front-facing
        rotationCompensation = (sensorOrientation + rotationCompensation) % 360;
      } else {
        // back-facing
        rotationCompensation =
            (sensorOrientation - rotationCompensation + 360) % 360;
      }
      rotation = InputImageRotationValue.fromRawValue(rotationCompensation);
      // print('rotationCompensation: $rotationCompensation');
    }
    if (rotation == null) return null;
    // print('final rotation: $rotation');

    // get image format
    final format = InputImageFormatValue.fromRawValue(image.format.raw);
    // validate format depending on platform
    // only supported formats:
    // * nv21 for Android
    // * bgra8888 for iOS
    if (format == null ||
        (Platform.isAndroid && format != InputImageFormat.nv21) ||
        (Platform.isIOS && format != InputImageFormat.bgra8888)) return null;

    // since format is constraint to nv21 or bgra8888, both only have one plane
    if (image.planes.length != 1) return null;
    final plane = image.planes.first;

    // compose InputImage using bytes
    return InputImage.fromBytes(
      bytes: plane.bytes,
      metadata: InputImageMetadata(
        size: Size(image.width.toDouble(), image.height.toDouble()),
        rotation: rotation, // used only in Android
        format: format, // used only in iOS
        bytesPerRow: plane.bytesPerRow, // used only in iOS
      ),
    );
  }
}
