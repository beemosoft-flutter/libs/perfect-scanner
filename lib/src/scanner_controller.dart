import 'dart:developer';
import 'dart:io';
import 'package:camera/camera.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:google_mlkit_barcode_scanning/google_mlkit_barcode_scanning.dart';
import 'package:image_picker/image_picker.dart';
import 'package:perfect_scanner/src/image_handler.dart';

class ScannerController {
  static bool _flash = false;
  static final ScannerController _singleton = ScannerController._internal();
  CameraController? _controller;

  factory ScannerController() {
    return _singleton;
  }

  ScannerController._internal() {
    _controller = CameraController(
      const CameraDescription(
        lensDirection: CameraLensDirection.back,
        name: 'Perfect Scanner',
        sensorOrientation: 90,
      ),
      ResolutionPreset.high,
      enableAudio: false,
      imageFormatGroup: Platform.isAndroid
          ? ImageFormatGroup.nv21
          : ImageFormatGroup.bgra8888,
    );
  }

  CameraController? get controller => _controller;

  void setCameraController(CameraController? controller) {
    _controller = controller;
  }

  /// Pick the image from gallery and get qr from that.
  ///
  /// Before calling this method gallery permission must be provided, otherwise permission exception will be thrown .
  ///
  /// This function will return a future string as a result
  static Future<String> getQrFromImage() async {
    String qrData = '';
    try {
      final file = await ImagePicker().pickImage(source: ImageSource.gallery);
      if (file != null) {
        final inputImage = InputImage.fromFilePath(file.path);
        debugPrint('ScanController getQrFromImage image path: ${file.path}');
        return await ImageHandler().processImage(inputImage) ?? "";
      } else {
        return '';
      }
    } catch (e) {
      log(
        e.toString(),
        name: 'SCANNER',
      );
    }
    return qrData;
  }

  ///Toggle the flash light while scanning.
  ///
  ///It will return a future bool value to know the flash status.
  Future<bool> toggleFlash() async {
    try {
      if (_flash == false) {
        _flash = true;
        await controller?.setFlashMode(FlashMode.torch);
      } else {
        _flash = false;
        await controller?.setFlashMode(FlashMode.off);
      }
    } catch (e) {
      log(
        e.toString(),
        name: 'SCANNER',
      );
    }
    return _flash;
  }

  ///It will pause the camera from scanning
  void pauseScanning() async {
    try {
      await controller?.pausePreview();
    } catch (e) {
      log(
        e.toString(),
        name: 'SCANNER',
      );
    }
  }

  ///It will resume the camera from scanning
  void resumeScanning() async {
    try {
      await controller?.resumePreview();
    } catch (e) {
      log(
        e.toString(),
        name: 'SCANNER',
      );
    }
  }

  ///It will take picture
  Future<XFile?> takePicture() async {
    try {
      /// Add below line if error: " java.lang.IllegalArgumentException: CaptureRequest contains unconfigured Input/Output Surface!"
      // controller.stopImageStream();
      XFile? result = await controller?.takePicture();
      return result;
    } catch (e) {
      log(
        e.toString(),
        name: 'TAKE PICTURE',
      );
      return null;
    }
  }

  /// Touch to focus
  Future<void> takeFocus(TapDownDetails details, BuildContext context) async {
    final x = details.localPosition.dx;
    final y = details.localPosition.dy;

    double fullWidth = MediaQuery.of(context).size.width;
    double cameraHeight = fullWidth * (controller?.value.aspectRatio ?? 1.0);

    final double relX = x / fullWidth;
    final double relY = y / cameraHeight;

    if (kDebugMode) {
      print("HoaLT: focus position x: $relX, y: $relY");
    }

    // Manually focus
    await controller?.setFocusPoint(Offset(relX, relY));
    await controller?.setFocusMode(FocusMode.locked);

    // Manually set light exposure
    controller?.setExposurePoint(Offset(relX, relY));
    controller?.setExposureMode(ExposureMode.locked);
  }

  Future<void> dispose() async {
    controller?.stopImageStream();
    controller?.dispose();
    setCameraController(null);
  }
}
